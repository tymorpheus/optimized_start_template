# SCSS Template

Author: Tymur Tolochko

Scss template is all-inclusive, optimized for Google PageSpeed start HTML5 template with Gulp, Scss, Autoprefixer (TODO: Clean-CSS, Uglify, Imagemin, Vinyl-FTP and Bower (libs path) support).

Scss Start Template uses the best practices of web development and optimized for Google PageSpeed, GTMetrix, Pingdom

Cross-browser compatibility: IE9+.

The template uses a Scss with __Scss__ syntax and project structure with source code in the directory __app/__ and production folder __dist/__, that contains ready project with optimized HTML, CSS, JS and images.

## How to use SASS Template

1. Clone  __optimized_start_template__ 
2. Install Node Modules: _npm i_
3. Run the template: _gulp watch_


##Gulp tasks:


* __watch__: Watch for the changes, add autoprefixer after scss compilation
* __gulp__: run default gulp task (sass, js, watch) for web development
* __build__: build project to __dist__ folder (cleanup, image optimize, removing unnecessary files)
* __deploy__: project deployment on the server from __dist__ folder via __FTP__
* __clearcache__: clear all gulp cache



##Installation:

* Install Node Modules: npm i
* bower install (jquery predefined)
* run in terminal: _gulp_




##Rules for working with the starting HTML template


1. For installing new jQuery library, just run the command "__bower i plugin-name__" in the terminal. Libraries are automatically placed in the folder __app/libs__. Bower must be installed in the system (npm i -g bower). Then place all jQuery libraries paths in the __'libs'__ task (gulpfile.js)
2. All custom JS located in __app/js/common.js__
3. All Sass vars placed in __app/sass_vars.sass__
4. All media queries placed in __app/sass/media.sass__
5. All js libraries placed in __app/sass/libs.sass__
